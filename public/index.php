<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| First we need to get an application instance. This creates an instance
| of the application / container and bootstraps the application so it
| is ready to receive HTTP / Console requests from the environment.
|
*/
//$files1 = fopen(__DIR__ . '/index.php', 'r');
//$files1 = fopen(__DIR__ . '/../storage/app/proink/jMpSMLhLBvF78WTkiiEdg3rz5TmgROgmGcJJI22I.jpeg', 'r');
//var_dump($files1);
$app = require __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$app->run();
