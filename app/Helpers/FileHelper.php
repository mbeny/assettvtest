<?php

namespace App\Helpers;

class FileHelper
{
    /**
     * @param $dir
     * @param $file
     * @return string
     */
    public static function getStoragePath($dir, $file)
    {
        return storage_path($dir) . DIRECTORY_SEPARATOR . $file;
    }
}