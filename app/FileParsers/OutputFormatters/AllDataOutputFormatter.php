<?php

namespace App\FileParsers\OutputFormatters;

use App\FileParsers\ParserOutputInterface;

class AllDataOutputFormatter implements ParserOutputInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function format($data)
    {
        return $data;
    }
}