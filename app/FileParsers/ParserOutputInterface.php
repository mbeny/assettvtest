<?php

namespace App\FileParsers;

interface ParserOutputInterface
{

    /**
     * @param $data
     */
    public function format($data);

}