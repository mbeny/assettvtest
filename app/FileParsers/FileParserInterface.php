<?php

namespace App\FileParsers;

interface FileParserInterface
{

    /**
     * @param $file
     */
    public function getFileInfo($file);

    /**
     * @param ParserOutputInterface $outputFormatter
     * @return mixed
     */
    public function getResult(ParserOutputInterface $outputFormatter);

}