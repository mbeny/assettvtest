<?php 

namespace App\FileParsers\GetIdParser;

use App\FileParsers\FileParserInterface;
use App\FileParsers\ParserOutputInterface;

class GetIdFileParser implements FileParserInterface
{
    private $parser;

    private $result;

    /**
     * GetIdParser constructor.
     *
     * @param \getID3 $getID3
     */
    public function __construct(\getID3 $getID3)
    {
        $this->parser = $getID3;
    }

    /**
     * @param $file
     */
    public function getFileInfo($file)
    {
        $this->result = $this->parser->analyze($file);
    }

    /**
     * @param ParserOutputInterface $outputFormatter
     * @return mixed
     */
    public function getResult(ParserOutputInterface $outputFormatter)
    {
        return $outputFormatter->format($this->result);
    }
}
