<?php

namespace App\Http\Controllers;

use App\FileParsers\FileParserInterface;
use App\FileParsers\OutputFormatters\AllDataOutputFormatter;
use App\Helpers\FileHelper;
use Illuminate\Http\Request;

class FileController extends Controller
{
    /**
     * directory where files are stored before analyse - storage/app/tmp
     */
    const STORAGE_DIRECTORY = 'tmp';

    /**
     * video file types accepted by getID3
     */
    const ALLOWED_FILE_TYPES = [
        'mimetypes:video/x-ms-wmv',
        'video/x-msvideo',
        'video/x-matroska',
        'video/quicktime',
        'application/x-mpegURL',
        'video/x-flv'
    ];

    /**
     * @var FileParserInterface
     */
    private $fileParser;

    /**
     * FileController constructor.
     * @param FileParserInterface $fileParser
     */
    public function __construct(FileParserInterface $fileParser)
    {   
        $this->fileParser = $fileParser;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleFile(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|'. join(',', self::ALLOWED_FILE_TYPES)
        ]);

        $file = $request->file('file')->storePublicly(self::STORAGE_DIRECTORY);

        $filePath = FileHelper::getStoragePath('app', $file);

        $this->fileParser->getFileInfo($filePath);

        unlink($filePath);

        return response()->json(
            $this->fileParser->getResult(new AllDataOutputFormatter)
        );
    }
}
