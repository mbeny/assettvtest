FROM php:7-fpm

WORKDIR /src

COPY . .

RUN apt update \    
           && docker-php-ext-install pdo pdo_mysql \
