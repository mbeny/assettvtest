<?php

class GetIdFileParserTest extends TestCase
{
    public function test_parser_instantiation()
    {
        $this->assertInstanceOf(
            \App\FileParsers\GetIdParser\GetIdFileParser::class,
            new \App\FileParsers\GetIdParser\GetIdFileParser(
                new \getID3()
            )
        );
    }

    /**
     * @expectedException TypeError
     */
    public function test_parser_instantiation_wrong_type()
    {
        $this->assertInstanceOf(
            \App\FileParsers\GetIdParser\GetIdFileParser::class,
            new \App\FileParsers\GetIdParser\GetIdFileParser(
                new \App\FileParsers\OutputFormatters\AllDataOutputFormatter()
            )
        );
    }

    /**
     * @expectedException ArgumentCountError
     */
    public function test_parser_instantiation_no_arguments()
    {
        $this->expectException(ArgumentCountError::class);
        $this->assertInstanceOf(
            \App\FileParsers\GetIdParser\GetIdFileParser::class,
            new \App\FileParsers\GetIdParser\GetIdFileParser()
        );
    }

    public function test_parser_property_set()
    {
        $obj = new \App\FileParsers\GetIdParser\GetIdFileParser(new \getID3());

        $reflector = new ReflectionClass(\App\FileParsers\GetIdParser\GetIdFileParser::class);
        $property = $reflector->getProperty('parser');
        $property->setAccessible(true);

        $this->assertInstanceOf(
            getID3::class,
            $this->readAttribute($obj, 'parser')
        );
    }

    /**
     * test if output is returned after file is analysed
     */
    public function test_if_file_analysed()
    {
        $obj = new \App\FileParsers\GetIdParser\GetIdFileParser(new \getID3());
        $obj->getFileInfo('/src/storage/app/tmp/cat.avi');

        $reflector = new ReflectionClass(\App\FileParsers\GetIdParser\GetIdFileParser::class);
        $property = $reflector->getProperty('result');
        $property->setAccessible(true);

        $result = $property->getValue($obj);

        $this->assertEquals($result['filename'], 'cat.avi');
        $this->assertEquals($result['filenamepath'], '/src/storage/app/tmp/cat.avi');
        $this->assertEquals($result['filesize'], 2560);
    }

    /**
     * error is being returned if trying to analyse not existing file
     */
    public function test_error_if_file_doesnt_exist()
    {
        $obj = new \App\FileParsers\GetIdParser\GetIdFileParser(new \getID3());
        $obj->getFileInfo('/src/storage/app/tmp/ca.avi');

        $reflector = new ReflectionClass(\App\FileParsers\GetIdParser\GetIdFileParser::class);
        $property = $reflector->getProperty('result');
        $property->setAccessible(true);

        $result = $property->getValue($obj);

        $this->assertTrue(!empty($result['error']));
    }
}