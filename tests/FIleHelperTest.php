<?php

use App\Helpers\FileHelper;

class FileHelperTest extends \TestCase
{
    public function test_file_Url_is_Correct()
    {
        $storageDir = 'app';
        $filePath = 'tmp/file.avi';

        $result = FileHelper::getStoragePath($storageDir, $filePath);

        $this->assertEquals('/src/storage/app/tmp/file.avi', $result);
    }
}