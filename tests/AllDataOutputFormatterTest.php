<?php

class AllDataOutputFormatterTest extends TestCase
{
    public function test_output_format_not_modified()
    {
        $testInput = [
            'prop1' => 1,
            'prop2' => 'test',
            'prop3' => 'abcd',
            'prop4' => 12
        ];

        $obj = new \App\FileParsers\OutputFormatters\AllDataOutputFormatter();

        $result = $obj->format($testInput);

        $this->assertEquals($testInput, $result);
    }
}